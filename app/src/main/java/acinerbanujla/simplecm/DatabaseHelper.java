package acinerbanujla.simplecm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, Contact.ContactDB.DATABASENAME, null, Contact.ContactDB.VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Contact.ContactDB.TABLENAME + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "fname VARCHAR(255) NOT NULL," +
                "lname VARCHAR(255) NOT NULL," +
                "email VARCHAR(255) NOT NULL," +
                "contact VARCHAR(255) NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + Contact.ContactDB.TABLENAME);
        onCreate(db);
    }

    public boolean insertData(Contact mContact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Contact.ContactDB.FIRSTNAME, mContact.getFname());
            contentValues.put(Contact.ContactDB.LASTNAME, mContact.getLname());
            contentValues.put(Contact.ContactDB.EMAIL, mContact.getEmail());
            contentValues.put(Contact.ContactDB.NUMBER, mContact.getContact());

            long result = db.insert(Contact.ContactDB.TABLENAME, null, contentValues);
            db.setTransactionSuccessful();
            if (result > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public Cursor CusrsorUpdate(String search) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Contact.ContactDB.TABLENAME + " WHERE " + Contact.ContactDB._ID + " = " + search, null);
        return cursor;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor rs = db.rawQuery("SELECT * FROM " + Contact.ContactDB.TABLENAME, null);
        return rs;
    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(Contact.ContactDB.TABLENAME, "_id = ?", new String[]{id});
    }

    public boolean UpdateData(String search, Contact mContact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Contact.ContactDB.FIRSTNAME, mContact.getFname());
        contentValues.put(Contact.ContactDB.LASTNAME, mContact.getLname());
        contentValues.put(Contact.ContactDB.EMAIL, mContact.getEmail());
        contentValues.put(Contact.ContactDB.NUMBER, mContact.getContact());

        long result = db.update(Contact.ContactDB.TABLENAME, contentValues,
                Contact.ContactDB._ID + "=?", new String[]{search});
        db.setTransactionSuccessful();
        if (result > 0) {
            db.endTransaction();
            db.close();
            return true;
        } else {
            db.endTransaction();
            db.close();
            return false;
        }

    }

}
